# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :budgetp_web,
  ecto_repos: [Budgetp.Repo],
  generators: [context_app: :budgetp]

# Configures the endpoint
config :budgetp_web, BudgetpWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "W6zPFXyI8todxm5ZBnFrSV7VUUN0c3vpwFqmakj912wAa38WhfvzSKGXmkkP4ymf",
  render_errors: [view: BudgetpWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: BudgetpWeb.PubSub, adapter: Phoenix.PubSub.PG2]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :format_encoders, json: Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
