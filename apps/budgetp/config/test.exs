use Mix.Config

# Configure your database
config :budgetp, Budgetp.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "budgetp_test",
  hostname: "postgres",
  pool: Ecto.Adapters.SQL.Sandbox
