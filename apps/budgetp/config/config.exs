use Mix.Config

config :budgetp, ecto_repos: [Budgetp.Repo]
config :ecto, :json_library, Jason

import_config "#{Mix.env()}.exs"
