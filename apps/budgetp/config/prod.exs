use Mix.Config

# Configure your database
config :budgetp, Budgetp.Repo,
  adapter: Ecto.Adapters.Postgres,
  ssl: true,
  pool_size: 1
