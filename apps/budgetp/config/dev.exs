use Mix.Config

config :ex_debug_toolbar,
  enable: true

# Configure your database
config :budgetp, Budgetp.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "budgetp_test",
  hostname: "localhost",
  pool_size: 1
