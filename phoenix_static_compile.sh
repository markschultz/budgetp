#!/bin/bash
cd $phoenix_dir
yarn --cwd assets/ deploy
mix "${phoenix_ex}.digest"
if mix help "${phoenix_ex}.digest.clean" 1>/dev/null 2>&1; then
  mix "${phoenix_ex}.digest.clean"
fi

