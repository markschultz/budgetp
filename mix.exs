defmodule Budgetp.Umbrella.MixProject do
  use Mix.Project

  def project do
    [
      apps_path: "apps",
      preferred_cli_env: [
        "phx.digest": :prod,
        release: :prod
      ],
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options.
  #
  # Dependencies listed here are available only for this project
  # and cannot be accessed from applications inside the apps folder
  defp deps do
    [
      {:distillery, "~> 1.5", runtime: false},
      {:ex_debug_toolbar, "~> 0.5.0"},
      {:jason, "~> 1.0"}
    ]
  end
end
